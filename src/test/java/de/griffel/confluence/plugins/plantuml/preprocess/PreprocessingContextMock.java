package de.griffel.confluence.plugins.plantuml.preprocess;

import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.api.model.content.ContentStatus;
import com.atlassian.confluence.api.model.content.Space;
import com.atlassian.confluence.api.service.content.ContentService;
import com.atlassian.confluence.api.service.content.SpaceService;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.renderer.ShortcutLinkConfig;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;

public class PreprocessingContextMock implements PreprocessingContext {

   public PageContext getPageContext() {
      return new PageContextMock();
   }

   public String getBaseUrl() {
      return "http:://localhost:8080/confluence";
   }

   public SpaceService getSpaceService() {
      final SpaceService spaceManager = mock(SpaceService.class);
      final Space puml = Space.builder().key("PUML").name("PlantUML Space").build();
      final SpaceService.SpaceFinder spaceFinder = mock(SpaceService.SpaceFinder.class);
      lenient().when(spaceFinder.withKeys(any())).thenReturn(spaceFinder);
      lenient().when(spaceFinder.fetchOrNull()).thenReturn(puml);
      lenient().when(spaceManager.find()).thenReturn(spaceFinder);
      return spaceManager;
   }

   @Override
   public ContentService getContentService() {
      final Content content = mock(Content.class);
      final ContentService contentService = mock(ContentService.class);
      final ContentService.ContentFinder contentFinder = mock(ContentService.ContentFinder.class);

      lenient().when(contentFinder.withSpace(any())).thenReturn(contentFinder);
      lenient().when(contentFinder.withType(any())).thenReturn(contentFinder);
      lenient().when(contentFinder.withTitle(anyString())).thenReturn(contentFinder);
      lenient().when(contentFinder.withStatus(any(ContentStatus.class))).thenReturn(contentFinder);
      lenient().when(contentFinder.fetchOrNull()).thenReturn(content);

      lenient().when(contentService.find()).thenReturn(contentFinder);

      return contentService;
   }

   public Map<String, ShortcutLinkConfig> getShortcutLinks() {
      return ImmutableMap.of("google",
            new ShortcutLinkConfig("http://www.google.com/search?q=", "Google Search with '%s'"));
   }

   public PageAnchorBuilder getPageAnchorBuilder() {
      return new PageAnchorBuilder();
   }

}
