/*
 * Copyright (C) 2011 Michael Griffel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * This distribution includes other third-party libraries.
 * These libraries and their corresponding licenses (where different
 * from the GNU General Public License) are enumerated below.
 *
 * PlantUML is a Open-Source tool in Java to draw UML Diagram.
 * The software is developed by Arnaud Roques at
 * http://plantuml.sourceforge.org.
 */
package de.griffel.confluence.plugins.plantuml.preprocess;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * JUnit Test for UrlReplaceFunctionTest.
 */
@ExtendWith(MockitoExtension.class)
public class UrlReplaceFunctionTest {
   private PreprocessingContext preprocessingContext = new PreprocessingContextMock();

   private String baseUrl = "http://localhost:8080/foo/bar";

   @Test
   public void testStandardSyntax() throws Exception {
      checkConfluenceUrl("url for Bob is [[Home]]", "/display/PUML/Home", "PlantUML Space - Home");
      checkConfluenceUrl("url for Bob is [[PUML:Home]]", "/display/PUML/Home", "PlantUML Space - Home");
      checkConfluenceUrl("url for Bob is [[PUML:Home|alias]]", "/display/PUML/Home", "alias");
      checkConfluenceUrl("url for Bob is [[PUML:Home|alias foo bar]]", "/display/PUML/Home", "alias foo bar");
      checExternalUrl("url for Bob is [[/foo/bar.html]]");
      checExternalUrl("url for Bob is [[https://www.example.com/secure]]");
      checExternalUrl("url for Bob is [[http://www.example.com/foo/bar.html]]");
      checExternalUrl("url for Bob is [[http://www.example.com/foo/bar.html|alias]]");
   }

   @Test
   public void testConfluenceSyntax() throws Exception {
      checkConfluenceUrl("url for Bob is [Home]", "/display/PUML/Home", "PlantUML Space - Home");
      checkConfluenceUrl("url for Bob is [alias foo bar|Home]", "/display/PUML/Home", "alias foo bar");
      checkConfluenceUrl("url for Bob is [alias foo bar|PUML:Home]", "/display/PUML/Home", "alias foo bar");
   }

   @Test
   public void testShortcutLinks() throws Exception {
      checkShortcutUrl("url for Bob is [foo@google]",
            "url for Bob is [[http://www.google.com/search?q=foo{Google Search with 'foo'}]]");
   }

   private void checkShortcutUrl(String line, String expected) throws Exception {
      assertEquals(expected, new UrlReplaceFunction().apply(preprocessingContext, line));
   }

   private void checkConfluenceUrl(String line, String result, String alias) throws Exception {
      assertEquals("url for Bob is " + "[[http:://localhost:8080/confluence" + result + "{" + alias + "}]]",
            new UrlReplaceFunction().apply(preprocessingContext, line));
   }

   private void checExternalUrl(String line) throws Exception {
      assertEquals(line, new UrlReplaceFunction().apply(preprocessingContext, line));
   }
}
