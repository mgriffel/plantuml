package de.griffel.confluence.plugins.plantuml;

import de.griffel.confluence.plugins.plantuml.PlantUmlMacroParams.Param;
import java.util.Collections;
import net.sourceforge.plantuml.core.DiagramType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class PlantUmlMacroParamsTest {
  @Nested
  @DisplayName("diagramType")
  public static class DiagramTypeTest {
    @Test
    public void unknownExplicitParam() {
      assertDiagramType("foo", "", DiagramType.UML);
      assertDiagramType("foo", "@startditaa", DiagramType.UML);
    }
    @Test
    public void explicitParam() {
      assertDiagramType("ditaa", "", DiagramType.DITAA);
      assertDiagramType("DITAA", "", DiagramType.DITAA);
      assertDiagramType("DITAA", "@startflow", DiagramType.DITAA);
    }
    @Test
    public void detected() {
      assertDiagramType(null, "", DiagramType.UML);
      assertDiagramType(null, "@startditaa", DiagramType.DITAA);
      assertDiagramType(null, "\n\n\n@startditaa", DiagramType.DITAA);
    }

    private void assertDiagramType(String param, String body, DiagramType expected) {
      PlantUmlMacroParams params = new PlantUmlMacroParams(
        Collections.singletonMap(Param.type.name(), param)
      );
      DiagramType actual = params.getDiagramType(body);
      Assertions.assertEquals(expected, actual);
    }
  }
}