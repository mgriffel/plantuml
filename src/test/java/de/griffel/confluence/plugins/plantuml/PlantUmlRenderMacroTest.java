package de.griffel.confluence.plugins.plantuml;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PlantUmlRenderMacroTest {

   @Test
   public void testHtml2TextHtmlBr() {
      final String given = "foo<br/>bar";
      final String text = PlantUmlRenderMacro.html2text(given);
      assertEquals("foo\n\nbar", text);
   }
   @Test
   public void testHtml2TextHtmlPara() {
      final String given = "foo<p/>bar";
      final String text = PlantUmlRenderMacro.html2text(given);
      assertEquals("foo\nbar", text);
   }

   @Test
   public void testFlowChartRender(){
      final String given = "\ndigraph g {\n" +
            "\nedge [arrowsize=\\\"0.8\\\"];" +
            "\nnode [shape=\\\"rect\\\", style=\\\"filled\\\", fillcolor=\\\"lightyellow\\\", fontname=\\\"Verdana\\\", fontsize=\\\"9\\\"];" +
            "\n" +
                  "<p><div class=\\\"conf-macro output-block\\\" data-hasbody=\\\"false\\\" data-macro-name=\\\"github-file-macro\\\"><p>\n" +
                  "    </p><div class=\\\"ac-content\\\">\n" +
                  "    <div class=\\\"mibex-include-file-container margin-right\\\" style=\\\"overflow:auto;\\\">\n" +
                  "        \n" +
                  "        <div class=\\\"file-header-container\\\">\n" +
                  "            \n" +
                  "\n" +
                  "            \n" +
                  "\n" +
                  "            <div class=\\\"collapsible-content\\\">\n" +
                  "                \n" +
                  "                \n" +
                  "                <div>\n" +
                  "                    \n" +
                  "                    <div>\n" +
                  "                        \n" +
                  "                        \n" +
                  "                        \n" +
                  "                        \n" +
                  "                        <div>\n" +
                  "                            \n" +
                  "                            \n" +
                  "                            \n" +
                  "                            <pre><code class=\\\"language-none\\\">Foo -&gt; Bar\n" +
                  "</code></pre>\n" +
                  "                        </div>\n" +
                  "                    </div>\n" +
                  "                </div>\n" +
                  "            </div>\n" +
                  "        </div>\n" +
                  "\n" +
                  "        \n" +
                  "    </div>\n" +
                  "</div></div></p>\n" +
                  "}\n";

      final String text = PlantUmlRenderMacro.html2text(given);

      assertEquals("\n" +
            "digraph g {\n" +
            "\n" +
            "edge [arrowsize=\\\"0.8\\\"];\n" +
            "node [shape=\\\"rect\\\", style=\\\"filled\\\", fillcolor=\\\"lightyellow\\\", fontname=\\\"Verdana\\\", fontsize=\\\"9\\\"];\n" +
            "\n" +
            "\n" +
            "\n" +
            "    \n" +
            "    \n" +
            "        \n" +
            "        \n" +
            "            \n" +
            "\n" +
            "            \n" +
            "\n" +
            "            \n" +
            "                \n" +
            "                \n" +
            "                \n" +
            "                    \n" +
            "                    \n" +
            "                        \n" +
            "                        \n" +
            "                        \n" +
            "                        \n" +
            "                        \n" +
            "                            \n" +
            "                            \n" +
            "                            \n" +
            "                            Foo -> Bar\n" +
            "\n" +
            "                        \n" +
            "                    \n" +
            "                \n" +
            "            \n" +
            "        \n" +
            "\n" +
            "        \n" +
            "    \n" +
            "\n" +
            "\n" +
            "}\n", text);
   }
}