package de.griffel.confluence.plugins.plantuml.preprocess;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlTimeoutException;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.impl.content.render.xhtml.analytics.MarshallerMetricsConsumer;
import com.atlassian.confluence.pages.ContentTree;
import com.atlassian.confluence.renderer.PageContext;
import io.atlassian.util.concurrent.Timeout;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Set;

public class ConversionContextMock implements ConversionContext {
   @Override
   public void setProperty(String s, Object o) {

   }

   @Override
   public PageContext getPageContext() {
      return new PageContextMock();
   }

   @Override
   public Object removeProperty(String s) {
      return null;
   }

   @Override
   public Object getProperty(String s) {
      return null;
   }

   @Override
   public Object getProperty(String s, Object o) {
      return null;
   }

   @Override
   public String getPropertyAsString(String s) {
      return null;
   }

   @Override
   public boolean hasProperty(String s) {
      return false;
   }

   @Override
   public ContentTree getContentTree() {
      return null;
   }

   @Override
   public String getOutputDeviceType() {
      return null;
   }

   @Override
   public @NonNull String getOutputType() {
      return null;
   }

   @Override
   public boolean isAsyncRenderSafe() {
      return false;
   }

   @Override
   public void disableAsyncRenderSafe() {

   }

   @Override
   public @Nullable ContentEntityObject getEntity() {
      return null;
   }

   @Override
   public String getSpaceKey() {
      return null;
   }

   @Override
   public Timeout timeout() {
      return null;
   }

   @Override
   public void checkTimeout() throws XhtmlTimeoutException {

   }

   @Override
   public boolean isDiffOrEmail() {
      return false;
   }

   @Override
   public void addMarshallerMetricsConsumer(MarshallerMetricsConsumer marshallerMetricsConsumer) {

   }

   @Override
   public boolean removeMarshallerMetricsConsumer(MarshallerMetricsConsumer marshallerMetricsConsumer) {
      return false;
   }

   @Override
   public Set<MarshallerMetricsConsumer> getMarshallerMetricsConsumers() {
      return null;
   }
}
