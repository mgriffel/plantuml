/*
 * Copyright (C) 2011 Michael Griffel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * This distribution includes other third-party libraries.
 * These libraries and their corresponding licenses (where different
 * from the GNU General Public License) are enumerated below.
 *
 * PlantUML is a Open-Source tool in Java to draw UML Diagram.
 * The software is developed by Arnaud Roques at
 * http://plantuml.sourceforge.org.
 */
package de.griffel.confluence.plugins.plantuml;

import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.api.model.content.ContentStatus;
import com.atlassian.confluence.api.model.content.Space;
import com.atlassian.confluence.api.service.accessmode.AccessModeService;
import com.atlassian.confluence.api.service.content.AttachmentService;
import com.atlassian.confluence.api.service.content.ContentService;
import com.atlassian.confluence.api.service.content.SpaceService;
import com.atlassian.confluence.importexport.resource.DownloadResourceReader;
import com.atlassian.confluence.importexport.resource.DownloadResourceWriter;
import com.atlassian.confluence.importexport.resource.WritableDownloadResourceManager;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.renderer.ShortcutLinkConfig;
import com.atlassian.confluence.renderer.ShortcutLinksManager;
import com.atlassian.confluence.setup.settings.Settings;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginInformation;
import com.google.common.collect.ImmutableMap;
import de.griffel.confluence.plugins.plantuml.config.PlantUmlConfiguration;
import de.griffel.confluence.plugins.plantuml.config.PlantUmlConfigurationManager;
import de.griffel.confluence.plugins.plantuml.preprocess.ConversionContextMock;
import de.griffel.confluence.plugins.plantuml.preprocess.PreprocessingContext;
import net.sourceforge.plantuml.core.DiagramType;
import net.sourceforge.plantuml.dot.GraphvizUtils;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

/**
 * Testing {@link de.griffel.confluence.plugins.plantuml.PlantUmlMacro}.
 * <p>
 * This unit test requires GraphViz.
 */
@ExtendWith(MockitoExtension.class)
public class PlantUmlMacroTest {
   private static final String BASE_URL = "http://localhost:8080/confluence";
   private static final String NEWLINE = "\n";
   @Mock
   private PluginAccessor pluginAccessor;

   @Mock
   private Plugin plugin;

   @Mock
   private PluginInformation pluginInfo;
   @Mock
   private SpaceService spaceService;
   @Mock
   private AttachmentService attachmentService;
   @Mock
   private ContentService contentService;
   @Mock
   private Space plantUmlSpaceMock;
   @Mock
   private PreprocessingContext preprocessingContext;
   @Mock
   private ShortcutLinksManager shortcutLinksManager;
   @Mock
   private PlantUmlConfigurationManager configurationManager;
   @Mock
   private XhtmlContent xhtmlContent;
   @Mock
   private AttachmentManager attachmentManager;
   @Mock
   private AccessModeService accessModeService;
   @Mock
   private SettingsManager settingsManager;
   @Mock
   private I18NBeanFactory i18NBeanFactory;
   @Mock
   private I18NBean i18NBean;
   @Mock
   private Content content;
   @Mock
   private ContentService.ContentFinder contentFinder;
   @Mock
   private SpaceService.SpaceFinder spaceFinder;

   @Test
   public void basic() throws Exception {
      when(settingsManager.getGlobalSettings()).thenReturn(new Settings() {
         public String getBaseUrl() {
            return BASE_URL;
         }
      });


      when(spaceFinder.withKeys(any())).thenReturn(spaceFinder);
      when(spaceFinder.fetchOrNull()).thenReturn(plantUmlSpaceMock);
      when(spaceService.find()).thenReturn(spaceFinder);
      when(plantUmlSpaceMock.getName()).thenReturn("PlantUML Space");

      when(contentFinder.withSpace(any())).thenReturn(contentFinder);
      when(contentFinder.withTitle(anyString())).thenReturn(contentFinder);
      when(contentFinder.withType(any())).thenReturn(contentFinder);
      when(contentFinder.withStatus(any(ContentStatus.class))).thenReturn(contentFinder);
      when(contentFinder.fetchOrNull()).thenReturn(content);
      when(contentService.find()).thenReturn(contentFinder);

      final ShortcutLinkConfig googleShortcutLinkConfig = new ShortcutLinkConfig();
      googleShortcutLinkConfig.setDefaultAlias("Google Search with '%s'");
      googleShortcutLinkConfig.setExpandedValue("http://www.google.com/search?q=%s");
      when(shortcutLinksManager.getShortcutLinks()).thenReturn(
            ImmutableMap.of("google", googleShortcutLinkConfig));
      final Map<String, ShortcutLinkConfig> shortcutLinks = shortcutLinksManager.getShortcutLinks();


      when(configurationManager.load()).thenReturn(PlantUmlConfiguration.builder()
            .fileFormatSvgDefault(false)
            .preserveWhiteSpaces(false)
            .build());


      Assumptions.assumeTrue(GraphvizUtils.getDotExe() != null);
      Assumptions.assumeTrue(!GraphvizUtils.dotVersion().startsWith("Error:"));

      final MockExportDownloadResourceManager resourceManager = new MockExportDownloadResourceManager();
      resourceManager.setDownloadResourceWriter(new MockDownloadResourceWriter());
      final PlantUmlMacro macro = new PlantUmlMacro(
            resourceManager,
            spaceService,
            attachmentService,
            contentService,
            settingsManager,
            pluginAccessor,
            shortcutLinksManager,
            configurationManager,
            i18NBeanFactory,
            xhtmlContent,
            attachmentManager,
            accessModeService);
      final Map<String, String> macroParams = ImmutableMap.<String, String>builder()
            .put(PlantUmlMacroParams.Param.title.name(), "Sample Title")
            .put(PlantUmlMacroParams.Param.type.name(), DiagramType.UML.name().toLowerCase())
            .build();
      final String macroBody = "A <|-- B\nurl for A is [[Home]]";
      final String result = macro.execute(macroParams, macroBody, new ConversionContextMock());
      StringBuilder sb = new StringBuilder();
      sb.append("<map id=\"x\" name=\"plantumlx_map\">");
      sb.append(NEWLINE);
      sb.append("<area shape=\"rect\" id=\"x\" href=\"x\" title=\"x\" alt=\"\" coords=\"x\"/>");
      sb.append(NEWLINE);
      sb.append("</map><span class=\"confluence-embedded-file-wrapper\" style=\"\">");
      sb.append("<img class=\"confluence-embedded-image\" usemap=\"#plantumlx_map\" src='junit/resource.png?contentType=image/png' style=\"\" /></span>");
      assertEquals(sb.toString(), result
            // GraphViz Version Specific
            .replaceAll("id=\"[^\"]*\"", "id=\"x\"")
            .replaceAll("plantuml[^\"]*_map\"", "plantumlx_map\"")
            .replaceFirst("href=\"[^\"]*\"", "href=\"x\"")
            .replaceFirst("title=\"[^\"]*\"", "title=\"x\"")
            .replaceFirst("coords=\"[^\"]*\"", "coords=\"x\""));
      final ByteArrayOutputStream out = (ByteArrayOutputStream) resourceManager.getResourceWriter(null, null, null)
            .getStreamForWriting();
      assertTrue(out.toByteArray().length > 0); // file size depends on installation of graphviz
      IOUtils.write(out.toByteArray(), new FileOutputStream("target/junit-basic.png"));
   }

   @Test
   public void ditaa() throws Exception {
      final ShortcutLinkConfig googleShortcutLinkConfig = new ShortcutLinkConfig();
      googleShortcutLinkConfig.setDefaultAlias("Google Search with '%s'");
      googleShortcutLinkConfig.setExpandedValue("http://www.google.com/search?q=%s");
      when(shortcutLinksManager.getShortcutLinks()).thenReturn(
            ImmutableMap.of("google", googleShortcutLinkConfig));
      final Map<String, ShortcutLinkConfig> shortcutLinks = shortcutLinksManager.getShortcutLinks();

      when(configurationManager.load()).thenReturn(PlantUmlConfiguration.builder()
            .fileFormatSvgDefault(false)
            .preserveWhiteSpaces(false)
            .build());

      final MockExportDownloadResourceManager resourceManager = new MockExportDownloadResourceManager();
      resourceManager.setDownloadResourceWriter(new MockDownloadResourceWriter());
      final PlantUmlMacro macro = new PlantUmlMacro(
            resourceManager,
            spaceService,
            attachmentService,
            contentService,
            settingsManager,
            pluginAccessor,
            shortcutLinksManager,
            configurationManager,
            i18NBeanFactory,
            xhtmlContent,
            attachmentManager,
            accessModeService);
      final ImmutableMap<String, String> macroParams =
            new ImmutableMap.Builder<String, String>()
                  .put(PlantUmlMacroParams.Param.type.name(), DiagramType.DITAA.name().toLowerCase())
                  .put(PlantUmlMacroParams.Param.align.name(), PlantUmlMacroParams.Alignment.center.name())
                  .put(PlantUmlMacroParams.Param.border.name(), "3")
                  .build();

      final String macroBody = new StringBuilder()
            .append("/--------\\   +-------+\n")
            .append("|cAAA    +---+Version|\n")
            .append("|  Data  |   |   V3  |\n")
            .append("|  Base  |   |cRED{d}|\n")
            .append("|     {s}|   +-------+\n")
            .append("\\---+----/\n").toString();

      final String result = macro.execute(macroParams, macroBody, new ConversionContextMock());

      assertEquals(
            "<span class=\"confluence-embedded-file-wrapper\" style=\"display: block; text-align: center;\"><img class=\"confluence-embedded-image\" src='junit/resource.png?contentType=image/png' " +
                  "style=\"border:3px solid black;\" /></span>",
            result);

      final ByteArrayOutputStream out = (ByteArrayOutputStream) resourceManager.getResourceWriter(null, null, null)
            .getStreamForWriting();
      assertTrue(out.toByteArray().length > 0); // file size depends on installation of graphviz
      IOUtils.write(out.toByteArray(), new FileOutputStream("target/junit-ditaat.png"));
   }

   @Test
   public void testAbout() throws Exception {
      when(i18NBeanFactory.getI18NBean()).thenReturn(i18NBean);
      when(i18NBean.getText(Matchers.anyString())).thenAnswer((Answer<String>) invocation -> {
         Object[] args = invocation.getArguments();
         return "__" + args[0] + "__";
      });

      when(pluginAccessor.getPlugin(PlantUmlPluginInfo.PLUGIN_KEY)).thenReturn(plugin);
      when(plugin.getPluginInformation()).thenReturn(pluginInfo);
      when(pluginInfo.getVersion()).thenReturn("1.x");
      when(pluginInfo.getVendorName()).thenReturn("Vendor");
      when(pluginInfo.getVendorUrl()).thenReturn("URL");
      when(pluginInfo.getDescription()).thenReturn("blabla");

      final ShortcutLinkConfig googleShortcutLinkConfig = new ShortcutLinkConfig();
      googleShortcutLinkConfig.setDefaultAlias("Google Search with '%s'");
      googleShortcutLinkConfig.setExpandedValue("http://www.google.com/search?q=%s");

      when(configurationManager.load()).thenReturn(PlantUmlConfiguration.builder()
            .fileFormatSvgDefault(false)
            .preserveWhiteSpaces(false)
            .build());

      final MockExportDownloadResourceManager resourceManager = new MockExportDownloadResourceManager();
      resourceManager.setDownloadResourceWriter(new MockDownloadResourceWriter());
      final PlantUmlMacro macro = new PlantUmlMacro(resourceManager,
            spaceService,
            attachmentService,
            contentService,
            settingsManager,
            pluginAccessor,
            shortcutLinksManager,
            configurationManager,
            i18NBeanFactory,
            xhtmlContent,
            attachmentManager,
            accessModeService);

      final ImmutableMap<String, String> macroParams = new ImmutableMap.Builder<String, String>().put(
            PlantUmlMacroParams.Param.type.name(), DiagramType.UML.name().toLowerCase()).build();

      final String macroBody = new StringBuilder()
            .append("@startuml\n")
            .append("about\n")
            .append("@enduml\n").toString();

      final String result = macro.execute(macroParams, macroBody, new ConversionContextMock());

      assertEquals(
            "<div style=\"margin: 20px 0 15px 0;\">blabla Version: <b>1.x</b> by Vendor. <a href=\"URL\">"
                  + "Plugin Homepage</a>__plugin.info.feedback__</div><span class=\"confluence-embedded-file-wrapper\" style=\"\">"
                  + "<img class=\"confluence-embedded-image\" src='junit/resource.png?contentType=image/png' style=\"\" /></span>", result);
      final ByteArrayOutputStream out = (ByteArrayOutputStream) resourceManager.getResourceWriter(null, null, null)
            .getStreamForWriting();
      assertTrue(out.toByteArray().length > 0); // file size depends on installation of graphviz
      IOUtils.write(out.toByteArray(), new FileOutputStream("target/junit-plantuml-about.png"));
   }

   @Test
   public void testVersionInfo() {
      assertTrue("@startuml\nversion\n@enduml\n".matches(PlantUmlPluginInfo.PLANTUML_VERSION_INFO_REGEX));
      assertTrue("@startuml\nabout\n@enduml\n".matches(PlantUmlPluginInfo.PLANTUML_VERSION_INFO_REGEX));
      assertTrue("\n@startuml\rversion\r@enduml\n".matches(PlantUmlPluginInfo.PLANTUML_VERSION_INFO_REGEX));
      assertTrue(" @startuml\r\nversion\r\n@enduml\n".matches(PlantUmlPluginInfo.PLANTUML_VERSION_INFO_REGEX));
      assertTrue("@startuml\r\n@startuml\r\nversion\r\n@enduml\n@enduml\n"
            .matches(PlantUmlPluginInfo.PLANTUML_VERSION_INFO_REGEX));
   }

   static class MockExportDownloadResourceManager implements WritableDownloadResourceManager {

      private DownloadResourceWriter downloadResourceWriter;

      public DownloadResourceReader getResourceReader(String arg0, String arg1, @SuppressWarnings("rawtypes") Map arg2) {
         throw new UnsupportedOperationException();
      }

      public boolean matches(String arg0) {
         throw new UnsupportedOperationException();
      }

      public DownloadResourceWriter getResourceWriter(String arg0, String arg1, String arg2) {
         return getDownloadResourceWriter();
      }

      public void setDownloadResourceWriter(DownloadResourceWriter downloadResourceWriter) {
         this.downloadResourceWriter = downloadResourceWriter;
      }

      public DownloadResourceWriter getDownloadResourceWriter() {
         return downloadResourceWriter;
      }

   }

   private static class MockDownloadResourceWriter implements DownloadResourceWriter {
      private final OutputStream buffer = new ByteArrayOutputStream();

      public OutputStream getStreamForWriting() {
         return buffer;
      }

      public String getResourcePath() {
         return "junit/resource.png";
      }
   }

}
