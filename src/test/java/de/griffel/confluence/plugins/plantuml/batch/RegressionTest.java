package de.griffel.confluence.plugins.plantuml.batch;

import com.google.common.base.Charsets;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.io.Files;
import de.griffel.confluence.plugins.plantuml.PlantUmlConfigBuilder;
import de.griffel.confluence.plugins.plantuml.PlantUmlMacro.MySourceStringReader;
import de.griffel.confluence.plugins.plantuml.PlantUmlMacroParams;
import net.sourceforge.plantuml.FileFormat;
import net.sourceforge.plantuml.preproc.Defines;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.function.Executable;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.fail;

public class RegressionTest {
   final File basedir = new File("src/test/resources/batch");

   @TestFactory
   public Collection<DynamicTest> testFiles() {
      final String[] files = basedir.list(new FilenameFilter() {
         @Override
         public boolean accept(File dir, String name) {
            return name.endsWith(".puml");
         }
      });

      final Collection<DynamicTest> tests = Lists.newArrayList();
      for(final String file : files) {
         tests.add(DynamicTest.dynamicTest("File '" + file + "'", new Executable() {
            @Override
            public void execute() throws Throwable {
               runTest(file);
            }
         }));
      }
      return tests;
   }

   private void runTest(String file) throws IOException {
      final String umlBlock = Files.asCharSource(new File(basedir, file), Charsets.UTF_8).read();

      final PlantUmlMacroParams macroParams = new PlantUmlMacroParams(Maps.newHashMap());
      final List<String> config = new PlantUmlConfigBuilder().build(macroParams);
      final MySourceStringReader reader = new MySourceStringReader(Defines.createEmpty(), umlBlock, config);
      final ByteArrayOutputStream os = new ByteArrayOutputStream();

      reader.renderImage(os, FileFormat.PNG);

      final File referenceFile = new File(basedir, file.replace(".puml", ".png"));
      final byte[] expectedData = FileUtils.readFileToByteArray(referenceFile);

      final boolean equals = Arrays.equals(expectedData, os.toByteArray());

      if (equals) {
         final File generatedFile = new File("target", file.replace(".puml", ".png"));
         FileUtils.writeByteArrayToFile(generatedFile, os.toByteArray());
         fail("Image for file '" + file + "'differ");
      }
   }

}
