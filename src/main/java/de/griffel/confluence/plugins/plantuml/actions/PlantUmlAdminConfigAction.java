package de.griffel.confluence.plugins.plantuml.actions;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.util.breadcrumbs.AdminActionBreadcrumb;
import com.atlassian.confluence.util.breadcrumbs.Breadcrumb;
import com.atlassian.confluence.util.breadcrumbs.BreadcrumbAware;
import com.atlassian.xwork.HttpMethod;
import com.atlassian.xwork.PermittedMethods;
import de.griffel.confluence.plugins.plantuml.PlantumlSecurityProfile;
import de.griffel.confluence.plugins.plantuml.config.PlantUmlConfiguration;
import de.griffel.confluence.plugins.plantuml.config.PlantUmlConfigurationManager;
import lombok.extern.slf4j.Slf4j;
import net.sourceforge.plantuml.security.SFile;
import net.sourceforge.plantuml.security.SecurityUtils;
import org.apache.struts2.interceptor.parameter.StrutsParameter;

import static com.google.common.base.MoreObjects.firstNonNull;

/**
 * XWork action for the administration UI.
 */
@Slf4j
public final class PlantUmlAdminConfigAction extends ConfluenceActionSupport implements BreadcrumbAware {
   private static final long serialVersionUID = 1L;
   private PlantUmlConfigurationManager configurationManager;
   private boolean isSvek;
   private boolean isPreserveWhiteSpaces;
   private boolean fileFormatSvgDefault;
   private String commonHeader;
   private String commonFooter;

   @StrutsParameter
   public void setSvek(boolean flag) {
      isSvek = flag;
   }

   public boolean isSvek() {
      return isSvek;
   }

   @StrutsParameter
   public void setPreserveWhiteSpaces(boolean preserveWhiteSpaces) {
      isPreserveWhiteSpaces = preserveWhiteSpaces;
   }

   public boolean isPreserveWhiteSpaces() {
      return isPreserveWhiteSpaces;
   }

   public boolean isFileFormatSvgDefault() {
      return fileFormatSvgDefault;
   }

   @StrutsParameter
   public void setFileFormatSvgDefault(boolean fileFormatSvgDefault) {
      this.fileFormatSvgDefault = fileFormatSvgDefault;
   }

   public String getCommonHeader() {
      return commonHeader;
   }

   @StrutsParameter
   public void setCommonHeader(String commonHeader) {
      this.commonHeader = commonHeader;
   }

   public String getCommonFooter() {
      return commonFooter;
   }

   @StrutsParameter
   public void setCommonFooter(String commonFooter) {
      this.commonFooter = commonFooter;
   }

   public void setConfigurationManager(PlantUmlConfigurationManager configurationManager) {
      this.configurationManager = configurationManager;
   }

   public String getSecurityProfile() {
      PlantumlSecurityProfile.init();
      return SecurityUtils.getSecurityProfile().name();
   }

   public String getSecurityPath() {
      PlantumlSecurityProfile.init();
      final SFile securityPath = SecurityUtils.getSecurityPath();
      return securityPath == null ? "" : securityPath.conv().getAbsolutePath();
   }

   /**
    * {@inheritDoc}
    */
   @PermittedMethods({HttpMethod.GET, HttpMethod.POST})
   public String load() {
      final PlantUmlConfiguration configuration = configurationManager.load();

      isSvek = configuration.isSvek();
      isPreserveWhiteSpaces = configuration.isPreserveWhiteSpaces();
      fileFormatSvgDefault = configuration.isFileFormatSvgDefault();
      commonHeader = firstNonNull(configuration.getCommonHeader(), "");
      commonFooter = firstNonNull(configuration.getCommonFooter(), "");

      log.debug("Loaded configuration {}", configuration);
      return SUCCESS;
   }

   /**
    * {@inheritDoc}
    */
   @PermittedMethods({HttpMethod.POST})
   public String save() {
      final PlantUmlConfiguration configuration = PlantUmlConfiguration.builder()
            .svek(isSvek)
            .preserveWhiteSpaces(isPreserveWhiteSpaces)
            .fileFormatSvgDefault(fileFormatSvgDefault)
            .commonHeader(commonHeader)
            .commonFooter(commonFooter)
            .build();

      configurationManager.save(configuration);

      log.debug("Saved configuration {}", configuration);
      addActionMessage(getText("plantuml.admin.config.saved"));
      return SUCCESS;
   }

   /**
    * {@inheritDoc}
    */
   public Breadcrumb getBreadcrumb() {
      return new AdminActionBreadcrumb(this);
   }

}
