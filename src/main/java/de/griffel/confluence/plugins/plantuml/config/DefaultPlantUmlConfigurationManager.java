package de.griffel.confluence.plugins.plantuml.config;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import static java.util.Optional.ofNullable;

/**
 * This class is responsible for loading and storing the configuration for this plugin.
 */
@Slf4j
@RequiredArgsConstructor
public final class DefaultPlantUmlConfigurationManager implements PlantUmlConfigurationManager {

   @ComponentImport
   private final PluginSettingsFactory pluginSettingsFactory;

   private static final String PLUGIN_STORAGE_KEY = "de.griffel.confluence.plugins.plantuml.config";

   public PlantUmlConfiguration load() {
      final PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
      final PlantUmlConfiguration config = PlantUmlConfiguration.builder()
            .svek(getBooleanOrDefault(pluginSettings, PlantUmlConfiguration.Key.SVEK, true))
            .fileFormatSvgDefault(getBooleanOrDefault(pluginSettings,  PlantUmlConfiguration.Key.FILE_FORMAT_SVG_DEFAULT, true))
            .commonHeader(getStringOrNull(pluginSettings,  PlantUmlConfiguration.Key.COMMON_HEADER))
            .commonFooter(getStringOrNull(pluginSettings,  PlantUmlConfiguration.Key.COMMON_FOOTER))
            .preserveWhiteSpaces(getBooleanOrDefault(pluginSettings,  PlantUmlConfiguration.Key.PRESERVE_WHITE_SPACES, false))
            .build();
      return config;
   }


   public void save(PlantUmlConfiguration config) {
      final PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
      pluginSettings.put(withPluginKey( PlantUmlConfiguration.Key.SVEK), booleanToString(config.isSvek()));
      pluginSettings.put(withPluginKey( PlantUmlConfiguration.Key.FILE_FORMAT_SVG_DEFAULT), booleanToString(config.isFileFormatSvgDefault()));
      pluginSettings.put(withPluginKey( PlantUmlConfiguration.Key.COMMON_HEADER), config.getCommonHeader());
      pluginSettings.put(withPluginKey( PlantUmlConfiguration.Key.COMMON_FOOTER), config.getCommonFooter());
      pluginSettings.put(withPluginKey( PlantUmlConfiguration.Key.PRESERVE_WHITE_SPACES), booleanToString(config.isPreserveWhiteSpaces()));
   }

   private String booleanToString(boolean value) {
      return Boolean.toString(value);
   }

   private static String withPluginKey(String subKey) {
      return PLUGIN_STORAGE_KEY + "." + subKey;
   }

   private static String getStringOrNull(PluginSettings pluginSettings, String subKey) {
      return (String) pluginSettings.get(PLUGIN_STORAGE_KEY + "." + subKey);
   }

   private static Boolean getBooleanOrDefault(PluginSettings pluginSettings, String subKey, boolean defaultValue) {
      return ofNullable(pluginSettings.get(PLUGIN_STORAGE_KEY + "." + subKey))
            .map(String.class::cast)
            .map(Boolean::valueOf)
            .orElse(defaultValue);
   }

}
