package de.griffel.confluence.plugins.plantuml;

import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.api.model.content.ContentBody;
import com.atlassian.confluence.api.model.content.ContentRepresentation;
import com.atlassian.confluence.util.HTMLSearchableTextUtil;
import lombok.experimental.UtilityClass;
import org.xml.sax.SAXException;

@UtilityClass
public class ContentUtils {
   public static String getContentAsStringWithoutMarkup(Content content) {

      final ContentBody contentBody = content.getBody().get(ContentRepresentation.STORAGE);

      final String rawText;
      try {
         rawText = HTMLSearchableTextUtil.stripTags(content.getTitle(), contentBody.getValue());
      } catch (SAXException e) {
         throw new RuntimeException(
               "Failed to strip tags from content " + content.getTitle() + " (" + content.getId() + ")", e);
      }
      return rawText;
   }
}
