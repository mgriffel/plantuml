/*
 * Copyright (C) 2011 Michael Griffel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * This distribution includes other third-party libraries.
 * These libraries and their corresponding licenses (where different
 * from the GNU General Public License) are enumerated below.
 *
 * PlantUML is a Open-Source tool in Java to draw UML Diagram.
 * The software is developed by Arnaud Roques at
 * http://plantuml.sourceforge.org.
 */
package de.griffel.confluence.plugins.plantuml;

import com.atlassian.confluence.api.model.Expansion;
import com.atlassian.confluence.api.model.Expansions;
import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.api.model.content.ContentStatus;
import com.atlassian.confluence.api.model.content.Space;
import com.atlassian.confluence.api.service.accessmode.AccessModeService;
import com.atlassian.confluence.api.service.content.AttachmentService;
import com.atlassian.confluence.api.service.content.ContentService;
import com.atlassian.confluence.api.service.content.SpaceService;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.importexport.resource.DownloadResourceNotFoundException;
import com.atlassian.confluence.importexport.resource.DownloadResourceReader;
import com.atlassian.confluence.importexport.resource.DownloadResourceWriter;
import com.atlassian.confluence.importexport.resource.UnauthorizedDownloadResourceException;
import com.atlassian.confluence.importexport.resource.WritableDownloadResourceManager;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.renderer.ShortcutLinkConfig;
import com.atlassian.confluence.renderer.ShortcutLinksManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.confluence.xhtml.api.MacroDefinitionHandler;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.core.exception.InfrastructureException;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.renderer.v2.macro.MacroException;
import com.google.common.base.Charsets;
import de.griffel.confluence.plugins.plantuml.PlantUmlMacro.MySourceStringReader.ImageInfo;
import de.griffel.confluence.plugins.plantuml.config.PlantUmlConfiguration;
import de.griffel.confluence.plugins.plantuml.config.PlantUmlConfigurationManager;
import de.griffel.confluence.plugins.plantuml.preprocess.PageAnchorBuilder;
import de.griffel.confluence.plugins.plantuml.preprocess.PageAnchorV4Builder;
import de.griffel.confluence.plugins.plantuml.preprocess.PlantUmlPreprocessor;
import de.griffel.confluence.plugins.plantuml.preprocess.PreprocessingContext;
import de.griffel.confluence.plugins.plantuml.preprocess.PreprocessingException;
import de.griffel.confluence.plugins.plantuml.preprocess.UmlSourceLocator;
import de.griffel.confluence.plugins.plantuml.type.ConfluenceLink;
import de.griffel.confluence.plugins.plantuml.type.ImageMap;
import de.griffel.confluence.plugins.plantuml.type.UmlSourceBuilder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import net.sourceforge.plantuml.BlockUml;
import net.sourceforge.plantuml.FileFormat;
import net.sourceforge.plantuml.FileFormatOption;
import net.sourceforge.plantuml.SourceStringReader;
import net.sourceforge.plantuml.core.Diagram;
import net.sourceforge.plantuml.core.DiagramType;
import net.sourceforge.plantuml.core.ImageData;
import net.sourceforge.plantuml.core.UmlSource;
import net.sourceforge.plantuml.preproc.Defines;
import org.apache.commons.io.HexDump;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.mutable.MutableInt;
import org.apache.commons.lang3.time.StopWatch;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * The Confluence PlantUML Macro.
 */
@Slf4j
@AllArgsConstructor
public class PlantUmlMacro implements Macro {
   private final WritableDownloadResourceManager writeableDownloadResourceManager;

   private final SpaceService spaceService;
   private final AttachmentService attachmentService;
   private final ContentService contentService;

   private final SettingsManager settingsManager;

   private final PluginAccessor pluginAccessor;

   private final ShortcutLinksManager shortcutLinksManager;

   private final PlantUmlConfigurationManager configurationManager;

   private final I18NBeanFactory i18NBeanFactory;

   private final XhtmlContent xhtmlContent;

   private final AttachmentManager attachmentManager;

   private final AccessModeService accessModeService;

   public BodyType getBodyType() {
      return BodyType.PLAIN_TEXT;
   }

   public final OutputType getOutputType() {
      return OutputType.INLINE;
   }


   public final PageAnchorBuilder createPageAnchorBuilder() {
      return new PageAnchorV4Builder();
   }

   public String execute(Map<String, String> params, String body, ConversionContext context)
         throws MacroExecutionException {
      try {
         return executeInternal(params, body, context);
      } catch (final IOException
                     | MacroException
                     | UnauthorizedDownloadResourceException
                     | DownloadResourceNotFoundException e) {
         throw new MacroExecutionException(e);
      }
   }

   protected final String executeInternal(Map<String, String> params, final String body,
                                          final ConversionContext conversionContext)
         throws MacroException, IOException, UnauthorizedDownloadResourceException, DownloadResourceNotFoundException {

      PlantumlSecurityProfile.init();

      final StopWatch stopWatch = new StopWatch();
      stopWatch.start();

      final PlantUmlMacroParams macroParams = new PlantUmlMacroParams(params);

      final PageContext pageContext = conversionContext.getPageContext();
      final UmlSourceLocator umlSourceLocator = new UmlSourceLocatorConfluence(pageContext);
      final PreprocessingContext preprocessingContext = new MyPreprocessingContext(pageContext);

      final DiagramType diagramType = macroParams.getDiagramType(body);
      final boolean dropShadow = macroParams.getDropShadow();
      final boolean separation = macroParams.getSeparation();
      final boolean preserveWhiteSpaces = macroParams.getPreserveWhiteSpaces();
      final PlantUmlConfiguration configuration = configurationManager.load();

      final UmlSourceBuilder umlSourceBuilder =
            new UmlSourceBuilder(diagramType, dropShadow, separation, preserveWhiteSpaces, configuration)
                  .append(new StringReader(body));

      final PlantUmlPreprocessor preprocessor =
            new PlantUmlPreprocessor(umlSourceBuilder.build(), umlSourceLocator, preprocessingContext);

      final String umlBlock = preprocessor.toUmlBlock();

      final String result = render(diagramType, umlBlock, pageContext, macroParams, preprocessor, configuration);

      stopWatch.stop();
      log.info(String.format("Rendering %s diagram on page %s:%s took %d ms.", diagramType,
            pageContext.getSpaceKey(), pageContext.getPageTitle(), stopWatch.getTime()));

      return result;
   }

   private String render(DiagramType diagramType, final String umlBlock, final PageContext pageContext, final PlantUmlMacroParams macroParams,
                         final PlantUmlPreprocessor preprocessor,
                         PlantUmlConfiguration configuration) throws IOException,
         UnauthorizedDownloadResourceException, DownloadResourceNotFoundException {

      final FileFormat defaultFileFormat = configuration.getDefaultFileFormat();
      final FileFormat fileFormat = macroParams.getFileFormat(pageContext, defaultFileFormat);

      final List<String> config = new PlantUmlConfigBuilder().build(macroParams);
      final MySourceStringReader reader = new MySourceStringReader(new Defines(), umlBlock, config);

      final StringBuilder sb = new StringBuilder();

      if (preprocessor.hasExceptions()) {
         final List<String> exceptionDetails = preprocessor.getExceptions()
               .stream()
               .map(PreprocessingException::getDetails)
               .toList();

         sb.append(renderErrors(exceptionDetails));
      }

      // DITAA diagrams do not support SVG output
      if (diagramType == DiagramType.DITAA && fileFormat == FileFormat.SVG) {
         sb.append(renderErrors("Ditaa requires PNG output format. Please change the 'Image Format' parameter to PNG."));
      }

      while (reader.hasNext()) {
         final DownloadResourceWriter resourceWriter = writeableDownloadResourceManager.getResourceWriter(
               AuthenticatedUserThreadLocal.getUsername(), "plantuml", fileFormat.getFileSuffix());

         final ImageInfo imageInfo = reader.renderImage(resourceWriter.getStreamForWriting(), fileFormat);
         final ImageMap cmap = imageInfo.getImageMap();

         if (cmap.isValid()) {
            sb.append(cmap.toHtmlString());
         }

         if (umlBlock.matches(PlantUmlPluginInfo.PLANTUML_VERSION_INFO_REGEX)) {
            sb.append(new PlantUmlPluginInfo(pluginAccessor, i18NBeanFactory.getI18NBean()).toHtmlString());
         }

         final DownloadResourceInfo resourceInfo;
         if (macroParams.getExportName() != null && !preprocessor.hasExceptions() && !accessModeService.isReadOnlyAccessModeEnabled()) {
            checkForDuplicateExportNames(pageContext, macroParams, sb);
            resourceInfo = attachImage(pageContext.getEntity(), macroParams, imageInfo, fileFormat, resourceWriter);
         } else {
            resourceInfo = new DefaultDownloadResourceInfo(writeableDownloadResourceManager, resourceWriter, fileFormat);
         }

         if (FileFormat.SVG == fileFormat) {
            final StringWriter sw = new StringWriter();
            IOUtils.copy(resourceInfo.getStreamForReading(), sw, Charsets.UTF_8);
            sb.append("<span class=\"plantuml-svg-image\">");
            sb.append(SvgUtil.trim(sw.getBuffer().toString()));
            sb.append("</span>");
         } else /* PNG */ {
            sb.append("<span class=\"confluence-embedded-file-wrapper\" style=\"")
                  .append(macroParams.getAlignment().getCssStyle())
                  .append("\">");
            sb.append("<img class=\"confluence-embedded-image\"");
            if (cmap.isValid()) {
               sb.append(" usemap=\"#");
               sb.append(cmap.getId());
               sb.append("\"");
            }
            sb.append(" src='");
            sb.append(resourceInfo.getDownloadPath());
            sb.append("'");
            sb.append(macroParams.getImageStyle());
            sb.append("/>");
            sb.append("</span>");
         }

      }

      if (macroParams.isDebug()) {
         sb.append("<div class=\"puml-debug\">");
         sb.append("<pre>");
         final ByteArrayOutputStream baos = new ByteArrayOutputStream();
         HexDump.dump(umlBlock.getBytes(StandardCharsets.UTF_8), 0, baos, 0);
         sb.append(StringEscapeUtils.escapeHtml4(baos.toString())); // HexDump class writer bytes with JVM default encoding
         sb.append("</pre>");
         sb.append("</div>");
      }

      return sb.toString();
   }

   private static String renderErrors(Collection<String> errors) {
      return renderErrors(errors.toArray(new String[0]));
   }

   private static String renderErrors(String ...  errors) {
      final StringBuilder sb = new StringBuilder();
      sb.append("<p class=\"error\">");
      for (String error : errors) {
         sb.append("<strong>");
         sb.append("plantuml: ");
         sb.append(error);
         sb.append("</strong><br/>");
      }
      sb.append("</p>");

      return sb.toString();
   }

   private void checkForDuplicateExportNames(final PageContext pageContext, PlantUmlMacroParams macroParams, StringBuilder sb) {
      if (System.getProperty("PLANTUML_DISABLE_EXPORT_NAME_CHECK") != null) {
         return;
      }

      final String exportName = macroParams.getExportName();
      final MutableInt numberOfDuplicateExportNames = new MutableInt(0);

      try {
         xhtmlContent.handleMacroDefinitions(pageContext.getEntity()
                     .getBodyAsString(), new DefaultConversionContext(pageContext),
               new MacroDefinitionHandler() {
                  public void handle(MacroDefinition macroDefinition) {
                     if (!"plantuml".equals(macroDefinition.getName())) {
                        return;
                     }

                     final PlantUmlMacroParams otherMacroParameters = new PlantUmlMacroParams(macroDefinition.getParameters());

                     if (exportName.equals(otherMacroParameters.getExportName())) {
                        numberOfDuplicateExportNames.increment();
                     }
                  }
               });
      } catch (XhtmlException e) {
         log.warn("Error during check for duplicate macro attribute exportNames", e);
      }

      if (numberOfDuplicateExportNames.getValue() > 1) {
         final I18NBean i18NBean = i18NBeanFactory.getI18NBean();
         sb.append("<div class=\"aui-message aui-message-warning\">");
         sb.append("<p class=\"title\"><strong>");
         sb.append(i18NBean.getText("plantuml.macro.params.exportName.duplicate.title"));
         sb.append("</strong></p>");
         sb.append("<p>");
         sb.append(i18NBean.getText("plantuml.macro.params.exportName.duplicate.message", new Object[]{StringEscapeUtils.escapeHtml4(exportName)}));
         sb.append("</p>");
         sb.append("</div>");
         sb.append("<p/>");
      }
   }

   private DownloadResourceInfo attachImage(final ContentEntityObject page, final PlantUmlMacroParams macroParams,
                                            ImageInfo imageInfo, final FileFormat fileFormat, final DownloadResourceWriter resourceWriter)
         throws UnauthorizedDownloadResourceException, DownloadResourceNotFoundException, IOException {

      final String attachmentName;
      if (imageInfo.isSplitImage()) {
         attachmentName = macroParams.getExportName() + "-" + imageInfo.getIndex() + fileFormat.getFileSuffix();
      } else {
         attachmentName = macroParams.getExportName() + fileFormat.getFileSuffix();
      }
      Attachment attachment = attachmentManager.getAttachment(page, attachmentName);

      final Attachment previousVersion;
      if (attachment == null) {
         previousVersion = null;
         attachment = new Attachment();
         attachment.setFileName(attachmentName);
         attachment.setMediaType("image/" + fileFormat.name().toLowerCase());
         attachment.setVersionComment("PlantUML Diagram (generated)");
      } else {
         try {
            previousVersion = (Attachment) attachment.clone();
         } catch (Exception e) {
            throw new InfrastructureException(e);
         }
      }

      final DownloadResourceReader resourceReader =
            writeableDownloadResourceManager.getResourceReader(AuthenticatedUserThreadLocal.getUsername(),
                  resourceWriter.getResourcePath(), Collections.emptyMap());

      if (previousVersion == null
            || previousVersion.getFileSize() != resourceReader.getContentLength()
            || !IOUtils.contentEquals(previousVersion.getContentsAsStream(), resourceReader.getStreamForReading())) {
         attachment.setFileSize(resourceReader.getContentLength());
         page.addAttachment(attachment);
         attachmentManager.saveAttachment(attachment, previousVersion,
               resourceReader.getStreamForReading());

         log.debug("Saved image as attachment {}", attachmentName);
      }
      return new AttachmentDownloadResourceInfo(settingsManager.getGlobalSettings().getBaseUrl(), attachment);
   }

   @Getter
   private final class MyPreprocessingContext implements PreprocessingContext {
      private final PageContext pageContext;

      /**
       * {@inheritDoc}
       */
      private MyPreprocessingContext(PageContext pageContext) {
         this.pageContext = pageContext;
      }

      /**
       * Returns the base URL from the global settings.
       *
       * @return the base URL from the global settings.
       */
      public String getBaseUrl() {
         final String baseUrl = settingsManager.getGlobalSettings().getBaseUrl();
         return baseUrl;
      }

      @Override
      public SpaceService getSpaceService() {
         return spaceService;
      }

      @Override
      public ContentService getContentService() {
         return contentService;
      }


      /**
       * {@inheritDoc}
       */
      public Map<String, ShortcutLinkConfig> getShortcutLinks() {
         return shortcutLinksManager.getShortcutLinks();
      }

      /**
       * {@inheritDoc}
       */
      public PageAnchorBuilder getPageAnchorBuilder() {
         return createPageAnchorBuilder();
      }
   }

   /**
    * Gets the UML source either from a Confluence page or from an attachment.
    */
   private final class UmlSourceLocatorConfluence implements UmlSourceLocator {
      private final PageContext pageContext;

      private UmlSourceLocatorConfluence(PageContext pageContext) {
         this.pageContext = pageContext;
      }

      public UmlSource get(String name) throws IOException {
         final ConfluenceLink.Parser parser = new ConfluenceLink.Parser(pageContext, spaceService, contentService);
         final ConfluenceLink confluenceLink = parser.parse(name);

         if (log.isDebugEnabled()) {
            log.debug("Link '{}' -> {}", name, confluenceLink);
         }

         final String spaceKey = confluenceLink.getSpaceKey();
         final String pageTitle = confluenceLink.getPageTitle();

         final Space space = spaceService.find().withKeys(spaceKey).fetchOrNull();
         checkNotNull(space, "Cannot find space with key " + spaceKey);

         final Content content = contentService.find(
                     new Expansion(
                           Content.Expansions.BODY,
                           new Expansions(new Expansion("storage"))
                     )
               )
               .withSpace(space)
               .withTitle(pageTitle)
               .withStatus(ContentStatus.CURRENT)
               .fetchOrNull();

         checkNotNull(content, "Cannot find content with title " + pageTitle + " in space with key " + spaceKey);

         // page cannot be null since it is validated before
         if (confluenceLink.hasAttachmentName()) {

            final Content attachmentContent = attachmentService.find()
                  .withContainerId(content.getId())
                  .withFilename(confluenceLink.getAttachmentName())
                  .fetchOrNull();

            if (attachmentContent == null) {
               throw new IOException("Cannot find attachment '" + confluenceLink.getAttachmentName()
                     + "' on page '" + pageTitle
                     + "' in space '" + confluenceLink.getSpaceKey() + "'");
            }

            final Attachment attachment =
                  attachmentManager.getAttachment(attachmentContent.getId().asLong());

            return new UmlSourceBuilder().append(attachment.getContentsAsStream()).build();

         } else {
            final String rawText = ContentUtils.getContentAsStringWithoutMarkup(content);
            return new UmlSourceBuilder().append(rawText).build();
         }
      }
   }

   /**
    * Extension to {@link SourceStringReader} to add the function to get the image map for the diagram.
    */
   public static class MySourceStringReader extends net.sourceforge.plantuml.SourceStringReader {
      private static final Random RANDOM = new Random();
      private final Diagram system;
      private int index = 0;

      /**
       * Creates extended version of {@link SourceStringReader}.
       *
       * @param defines defines
       * @param source  plant uml source
       * @param config  configuration
       */
      public MySourceStringReader(Defines defines, String source, List<String> config) {
         super(defines, source, config);
         final BlockUml blockUml = getBlocks().iterator().next();
         system = blockUml.getDiagram();
      }

      public boolean hasNext() {
         return index < system.getNbImages();
      }

      public final ImageInfo renderImage(OutputStream outputStream, FileFormat format) throws IOException {
         final ImageData imageData = system.exportDiagram(outputStream, index, new FileFormatOption(format));

         final ImageMap imageMap;
         if (imageData.containsCMapData()) {
            final String randomId = String.valueOf(Math.abs(RANDOM.nextInt()));
            imageMap = new ImageMap(imageData.getCMapData("plantuml" + randomId));
         } else {
            imageMap = ImageMap.NULL;
         }
         return new ImageInfo(imageMap, index++);
      }

      @Getter
      public final class ImageInfo {
         private final ImageMap imageMap;
         private final int index;

         ImageInfo(ImageMap imageMap, int index) {
            this.imageMap = imageMap;
            this.index = index;
         }

         public boolean isSplitImage() {
            return system.getNbImages() > 1;
         }
      }
   }

}
