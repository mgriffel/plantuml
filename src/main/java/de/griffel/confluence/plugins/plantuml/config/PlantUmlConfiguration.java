package de.griffel.confluence.plugins.plantuml.config;

import lombok.Builder;
import lombok.Value;
import net.sourceforge.plantuml.FileFormat;

/**
 * Configuration properties.
 */
@Value
@Builder
public class PlantUmlConfiguration {

   public static class Key {
      public static final String SVEK = "svek";
      public static final String FILE_FORMAT_SVG_DEFAULT = "fileFormatSvgDefault";
      public static final String COMMON_HEADER = "commonHeader";
      public static final String COMMON_FOOTER = "commonFooter";
      public static final String PRESERVE_WHITE_SPACES = "preserveWhiteSpaces";
   }

   @Builder.Default
   boolean svek = true;
   boolean fileFormatSvgDefault;
   String commonHeader;
   String commonFooter;
   boolean preserveWhiteSpaces;

   public FileFormat getDefaultFileFormat() {
      return fileFormatSvgDefault ? FileFormat.SVG : FileFormat.PNG;
   }
}
