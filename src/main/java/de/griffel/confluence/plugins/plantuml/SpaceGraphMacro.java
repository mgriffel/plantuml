/*
 * Copyright (C) 2011 Michael Griffel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * This distribution includes other third-party libraries.
 * These libraries and their corresponding licenses (where different
 * from the GNU General Public License) are enumerated below.
 *
 * PlantUML is a Open-Source tool in Java to draw UML Diagram.
 * The software is developed by Arnaud Roques at
 * http://plantuml.sourceforge.org.
 */
package de.griffel.confluence.plugins.plantuml;

import com.atlassian.confluence.api.service.accessmode.AccessModeService;
import com.atlassian.confluence.api.service.content.AttachmentService;
import com.atlassian.confluence.api.service.content.ContentService;
import com.atlassian.confluence.api.service.content.SpaceService;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.importexport.resource.WritableDownloadResourceManager;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.renderer.ShortcutLinksManager;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.plugin.PluginAccessor;
import de.griffel.confluence.plugins.plantuml.config.PlantUmlConfigurationManager;

import java.util.Map;

/**
 * This is the {spacegraph} Macro.
 */
public class SpaceGraphMacro implements Macro {
   private final PlantUmlMacro plantUmlMacro;
   private final SpaceManager spaceManager;
   private final PageManager pageManager;
   private final SettingsManager settingsManager;
   private final PermissionManager permissionManager;
   private final ContentPropertyManager contentPropertyManager;

   public SpaceGraphMacro(WritableDownloadResourceManager writeableDownloadResourceManager,
                          PageManager pageManager,
                          SpaceManager spaceManager,
                          SpaceService spaceService,
                          AttachmentService attachmentService,
                          ContentService contentService,
                          SettingsManager settingsManager,
                          PluginAccessor pluginAccessor,
                          ShortcutLinksManager shortcutLinksManager,
                          PlantUmlConfigurationManager configurationManager,
                          PermissionManager permissionManager,
                          ContentPropertyManager contentPropertyManager,
                          I18NBeanFactory i18NBeanFactory,
                          XhtmlContent xhtmlContent,
                          AttachmentManager attachmentManager,
                          AccessModeService accessModeService) {
      this.spaceManager = spaceManager;
      this.pageManager = pageManager;
      this.settingsManager = settingsManager;
      this.permissionManager = permissionManager;
      this.contentPropertyManager = contentPropertyManager;

      plantUmlMacro =
            new PlantUmlMacro(
                  writeableDownloadResourceManager,
                  spaceService,
                  attachmentService,
                  contentService,
                  settingsManager,
                  pluginAccessor,
                  shortcutLinksManager,
                  configurationManager,
                  i18NBeanFactory,
                  xhtmlContent,
                  attachmentManager,
                  accessModeService
            );
   }

   public String execute(Map<String, String> params, String body, ConversionContext context)
         throws MacroExecutionException {
      return new AbstractLinkAndSpaceGraphMacroImpl() {
         @Override
         protected String executePlantUmlMacro(Map<String, String> params, String dotString, ConversionContext context)
               throws MacroExecutionException {

            final String realString =
                  createDotForSpaceGraph(
                        params,
                        context.getPageContext(),
                        spaceManager,
                        pageManager,
                        settingsManager,
                        permissionManager,
                        contentPropertyManager
                  );

            return plantUmlMacro.execute(params, realString, context);
         }
      }.execute(params, body, context);
   }

   public BodyType getBodyType() {
      return BodyType.NONE;
   }

   public OutputType getOutputType() {
      return plantUmlMacro.getOutputType();
   }

}
