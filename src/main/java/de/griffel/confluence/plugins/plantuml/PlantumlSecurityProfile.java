package de.griffel.confluence.plugins.plantuml;

import com.google.common.base.Strings;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import net.sourceforge.plantuml.security.SecurityUtils;

@UtilityClass
@Slf4j
public class PlantumlSecurityProfile {
   public static synchronized void init() {
      final String securityProfile = SecurityUtils.getenv("PLANTUML_SECURITY_PROFILE");

      if (Strings.isNullOrEmpty(securityProfile)) {
         final String securityProfileValue = "INTERNET";

         log.warn("Setting PLANTUML_SECURITY_PROFILE to {}. "
                     + "The security profile can be changed by setting the environment variable PLANTUML_SECURITY_PROFILE."
                     + "See https://plantuml.com/en/security for more details."
               , securityProfileValue);

         System.setProperty("PLANTUML_SECURITY_PROFILE", securityProfileValue);
      }
   }
}
