/*
 * Copyright (C) 2011 Michael Griffel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * This distribution includes other third-party libraries.
 * These libraries and their corresponding licenses (where different
 * from the GNU General Public License) are enumerated below.
 *
 * PlantUML is a Open-Source tool in Java to draw UML Diagram.
 * The software is developed by Arnaud Roques at
 * http://plantuml.sourceforge.org.
 */
package de.griffel.confluence.plugins.plantuml;

import com.atlassian.confluence.api.service.accessmode.AccessModeService;
import com.atlassian.confluence.api.service.content.AttachmentService;
import com.atlassian.confluence.api.service.content.ContentService;
import com.atlassian.confluence.api.service.content.SpaceService;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.importexport.resource.DownloadResourceNotFoundException;
import com.atlassian.confluence.importexport.resource.UnauthorizedDownloadResourceException;
import com.atlassian.confluence.importexport.resource.WritableDownloadResourceManager;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.renderer.ShortcutLinksManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.renderer.v2.macro.MacroException;
import de.griffel.confluence.plugins.plantuml.config.PlantUmlConfigurationManager;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.Map;


/**
 * PlantUmlMacro for Confluence V 4.x.
 */
public class PlantUmlRenderMacro extends PlantUmlMacro {

   private static final String HTML_BR = "§§html-br§§";
   private static final String HTML_PARA = "§§html-para§§";

   public PlantUmlRenderMacro(WritableDownloadResourceManager writeableDownloadResourceManager,
                              SpaceService spaceService,
                              AttachmentService attachmentService,
                              ContentService contentService,
                              SettingsManager settingsManager,
                              PluginAccessor pluginAccessor,
                              ShortcutLinksManager shortcutLinksManager,
                              PlantUmlConfigurationManager configurationManager,
                              I18NBeanFactory i18NBeanFactory,
                              XhtmlContent xhtmlContent,
                              AttachmentManager attachmentManager,
                              AccessModeService accessModeService) {
      super(writeableDownloadResourceManager,
            spaceService,
            attachmentService,
            contentService,
            settingsManager,
            pluginAccessor,
            shortcutLinksManager,
            configurationManager,
            i18NBeanFactory,
            xhtmlContent,
            attachmentManager,
            accessModeService);
   }

   public final BodyType getBodyType() {
      return BodyType.RICH_TEXT;
   }


   public String execute(Map<String, String> params, String bodyAsHtml, ConversionContext context)
         throws MacroExecutionException {
      try {

         final String bodyAsText = html2text(bodyAsHtml);

         return executeInternal(params, bodyAsText, context);
      } catch (final IOException
                     | MacroException
                     | UnauthorizedDownloadResourceException
                     | DownloadResourceNotFoundException e) {
         throw new MacroExecutionException(e);
      }
   }

   static String html2text(String bodyAsHtml) {
      final Document doc = Jsoup.parse(bodyAsHtml);
      doc.select("br").append(HTML_BR);
      doc.select("p").prepend(HTML_PARA);

      final String bodyAsText = doc.wholeText()
            .replaceAll(HTML_BR, "\n")
            .replaceAll(HTML_PARA, "\n");

      return bodyAsText;
   }

}
