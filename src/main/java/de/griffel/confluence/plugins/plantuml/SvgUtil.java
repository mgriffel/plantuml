package de.griffel.confluence.plugins.plantuml;

import org.apache.commons.lang3.StringUtils;

public final class SvgUtil {

   private SvgUtil() {
      throw new IllegalAccessError("No instances allowed. This class holds only static methods.");
   }

   /**
    * Returns a string where everything before and after the SVG XML element is removed.
    *
    * @param svgString the string containing the SVG.
    * @return
    */
   public static String trim(final String svgString) {
      final String result;

      if (svgString != null) {
         final String trimBefore = "<svg" + StringUtils.substringAfter(svgString, "<svg");
         result = StringUtils.substringBefore(trimBefore, "</svg>") + "</svg>";
      } else {
         result = svgString;
      }

      return result;
   }
}
