FROM atlassian/confluence-server:7.1-ubuntu-18.04-adoptopenjdk8

LABEL Description="This image is used to start Atlassian Confluence with PlantUML Plugin Support (Graphviz)" Vendor="mgriffel" Version=7.1.0

MAINTAINER mgriffel <michael.griffel@gmail.com>

RUN set -x \
	&& apt-get update \
	&& apt-get install -y \
	fonts-freefont-ttf \
	graphviz

